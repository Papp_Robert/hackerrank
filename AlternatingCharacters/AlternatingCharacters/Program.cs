﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AlternatingCharacters
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            alternatingCharacters("");
            Console.ReadLine();
        }

        static int alternatingCharacters(string s)
        {
            int deletionNumber = 0;
            for (int i = 0; i < s.Length - 1; i++)
            {
                if(s[i] == s[i+1])
                {
                    deletionNumber++;
                }
            }

            return deletionNumber;
        }
    }
}
