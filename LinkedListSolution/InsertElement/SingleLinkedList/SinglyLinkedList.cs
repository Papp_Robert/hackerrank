﻿using System;
using System.Collections.Generic;

namespace InsertElement
{
    public class SinglyLinkedList
    {
        public SinglyLinkedListNode head;
        public SinglyLinkedListNode tail;

        public SinglyLinkedList()
        {
            this.head = null;
            this.tail = null;
        }

        public void InsertNode(int nodeData)
        {
            SinglyLinkedListNode node = new SinglyLinkedListNode(nodeData);

            if (this.head == null)
            {
                this.head = node;
            }
            else
            {
                this.tail.next = node;
            }

            this.tail = node;
        }

        public SinglyLinkedListNode InsertNodeAtPosition(SinglyLinkedListNode head, int data, int position)
        {
            SinglyLinkedListNode node = new SinglyLinkedListNode(data);

            if (head == null)
            {
                return node;
            }

            SinglyLinkedListNode elementBefore = head.next;
            SinglyLinkedListNode elementAfter = elementBefore.next;

            for (int i = 1; i < position -1 ; i++)
            {                
                elementBefore = elementBefore.next;
                elementAfter = elementBefore.next;
            }
            elementBefore.next = node;

            node.next = elementAfter;

            return head;
        }

        public bool HasCycle(SinglyLinkedListNode head)
        {
            HashSet<SinglyLinkedListNode> hsSet = new HashSet<SinglyLinkedListNode>();

            while (head != null)
            {
                if (hsSet.Contains(head))
                {
                    return true;
                }
                hsSet.Add(head);

                head = head.next;
            }
            return false;
        }

        public int FindMergeNode(SinglyLinkedListNode head1, SinglyLinkedListNode head2)
        {
            SinglyLinkedListNode currentA = head1;
            SinglyLinkedListNode currentB = head2;

            //Do till the two nodes are the same
            while (currentA != currentB)
            {
                //If you reached the end of one list start at the beginning of the other one
                //currentA
                if (currentA.next == null)
                {
                    currentA = head2;
                }
                else
                {
                    currentA = currentA.next;
                }
                //currentB
                if (currentB.next == null)
                {
                    currentB = head1;
                }
                else
                {
                    currentB = currentB.next;
                }
            }
            return currentB.data;
        }
    }
}
