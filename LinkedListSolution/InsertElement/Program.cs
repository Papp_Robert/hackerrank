﻿using System;
using InsertElement.DoubleLinkedList;

namespace InsertElement
{
    public class Program
    {

        static void Main(string[] args)
        {
            //SinglyLinkedList list = new SinglyLinkedList();

            //SinglyLinkedListNode head = new SinglyLinkedListNode(16);
            //var secondElement =  new SinglyLinkedListNode(13);
            //head.next = secondElement;

            //var thirdElement = new SinglyLinkedListNode(7);
            //secondElement.next = thirdElement;

            //var fourthElement = new SinglyLinkedListNode(20);
            //thirdElement.next = fourthElement;

            //var fifthElement = new SinglyLinkedListNode(10);
            //fourthElement.next = fifthElement;

            //var sixthElement = new SinglyLinkedListNode(4);
            //fifthElement.next = sixthElement;

            //list.InsertNodeAtPosition(head, 1, 2);
            DoublyLinkedListNode firstNode = new DoublyLinkedListNode(2);
            DoublyLinkedList doubleLinkedList = new DoublyLinkedList();
            firstNode.prev = null;            

            DoublyLinkedListNode secondNode = new DoublyLinkedListNode(3);
            secondNode.prev = firstNode;
            firstNode.next = secondNode;
            secondNode.next = null;

            doubleLinkedList.InsertNode(firstNode.data);
            doubleLinkedList.InsertNode(secondNode.data);

            doubleLinkedList.Reverse(firstNode);
        }
    }
}
