﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace InsertElement.DoubleLinkedList
{
    public class DoublyLinkedList
    {
        public DoublyLinkedListNode head;
        public DoublyLinkedListNode tail;

        public DoublyLinkedList()
        {
            this.head = null;
            this.tail = null;
        }

        public void InsertNode(int nodeData)
        {
            DoublyLinkedListNode node = new DoublyLinkedListNode(nodeData);

            if (this.head == null)
            {
                this.head = node;
            }
            else
            {
                this.tail.next = node;
                node.prev = this.tail;
            }

            this.tail = node;
        }

        public void PrintDoublyLinkedList(DoublyLinkedListNode node, string sep, TextWriter textWriter)
        {
            while (node != null)
            {
                textWriter.Write(node.data);

                node = node.next;

                if (node != null)
                {
                    textWriter.Write(sep);
                }
            }
        }

        public DoublyLinkedListNode Reverse(DoublyLinkedListNode head)
        {
            var temp = head;
            var newHead = head;

            while (temp != null)
            {
                var prev = temp.prev;
                temp.prev = temp.next;
                temp.next = prev;
                newHead = temp;
                temp = temp.prev;
            }
            return newHead;
        }
    }


}
