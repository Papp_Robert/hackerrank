﻿using System;
using System.Numerics;

namespace FibonacciModified
{
    /// <summary>
    /// https://www.hackerrank.com/challenges/fibonacci-modified/problem
    /// </summary>
    class Program
    {
        static BigInteger fibonacciModified(int t1, int t2, int n)
        {
            BigInteger[] fibonacciModified = new BigInteger[n];
            fibonacciModified[0] = (uint)t1;
            fibonacciModified[1] = (uint)t2;

            for (int i = 2; i < n; i++)
            {
                fibonacciModified[i] = fibonacciModified[i - 2] + (fibonacciModified[i - 1] * fibonacciModified[i - 1]);
            }

            return fibonacciModified[n-1];
        }

        static void Main(String[] args)
        {
            BigInteger result = fibonacciModified(0, 1, 10);
            Console.WriteLine(result);
        }
    }
}
