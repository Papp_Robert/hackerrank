﻿using System.Collections.Generic;

namespace IEnumerableApproach
{
    public interface IOperation<T> 
    {
        IEnumerable<T> Execute(IEnumerable<T> input);
    }
}
