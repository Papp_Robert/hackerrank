﻿using System.Diagnostics;

namespace IEnumerableApproach
{
    public class TrivialProcessesPipeline : Pipeline<Process>
    {
        public TrivialProcessesPipeline()
        {
            Register(new GetAllProcesses());
            Register(new LimitByWorkingSetSize());
            Register(new PrintProcessName());

            Execute();
        }

    }
}
