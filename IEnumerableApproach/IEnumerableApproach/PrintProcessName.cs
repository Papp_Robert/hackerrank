﻿using System.Collections.Generic;
using System.Diagnostics;

namespace IEnumerableApproach
{
    /// <summary>
    /// Prints out the process name
    /// </summary>
    public class PrintProcessName : IOperation<Process>
    {
        public IEnumerable<Process> Execute(IEnumerable<Process> input)
        {
            foreach (var process in input)
            {
                System.Console.WriteLine(process.ProcessName);
            }
            yield break;
        }
    }
}
