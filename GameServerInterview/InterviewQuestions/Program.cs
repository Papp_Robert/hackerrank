﻿using System;
using System.Drawing;
using System.Globalization;
using System.Linq;

namespace InterviewQuestions
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");


            //var a = IsPalindrome("gorog");

            //var numb = ConvertDecimalNumberToTwoDecimalPlaces(5.6789);
            //var number = CountTheMultiplierNumbers(new int[] { 4, 6, 24 });

          // FormatNumber(1234256.583);

            Console.ReadLine();
        }
        public static bool IsPalindrome(string s)
        {
            string reversedString = new string(s.Reverse().ToArray());
            return reversedString == s;
        }

        public static double ConvertDecimalNumberToTwoDecimalPlaces(double number)
        {
            return Math.Floor(100 * number) / 100;
        }

        public static int CountTheMultiplierNumbers(int [] numbers)
        {
            int multipleCount = 0;

            foreach (var number in numbers)
            {
                if(number % 4 == 0 || number % 6 == 0)
                {
                    multipleCount++;
                }
            }

            return multipleCount;
        }

        public static string FormatNumber(double number)
        {
            return string.Format(new CultureInfo("en-GB"), "{0:0,0.00}", number);
        }

        public static void MoveFirstObjectCloserToTheSecondOne(Point firstPoint, Point secondPoint)
        {
            int newX = 0, newY = 0;
            if(firstPoint.X > secondPoint.X)
            {
                newX = firstPoint.X + 1; 
            }
            else
            {
                newX = firstPoint.X - 1;
            }

            if (firstPoint.Y > secondPoint.Y)
            {
                newY = firstPoint.Y + 1;
            }
            else
            {
                newY = firstPoint.Y - 1;
            }

            firstPoint = new Point(newX, newY);
        }
    }
}
