﻿using System;
using System.Linq;

namespace GreedyFloristProblem
{
    class Program
    {
        static int getMinimumCost(int n, int k, int[] c)
        {
            int count = 0;
            c = c.OrderBy(x => x).ToArray();

            if (k == n)
            {
                count += c.Sum();
            }
            if (n > k)
            {
                //while (n != 0)
                //{
                //    int numbersToBuyPerMen = n / k;

                //    for (int i = 1; i <= numbersToBuyPerMen; i++)
                //    {                       
                //            var max = c.Max();
                //            c = c.Where(x => x != max).ToArray();

                //            count += max * i;
                //            n--;
                //    }
                //    k--;
                //}
                for (int i = n - 1; i >= 0; i--)
                {
                    count += c[i] * ((n - i - 1) / k + 1);
                }
            }

            return count;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            getMinimumCost(50, 3,
                new int[] { 390225, 426456, 688267, 800389, 990107, 439248, 240638, 15991, 874479, 568754,
                    729927, 980985, 132244, 488186, 5037, 721765, 251885, 28458, 23710, 281490, 30935, 897665,
                    768945, 337228, 533277, 959855, 927447, 941485, 24242, 684459, 312855, 716170, 512600, 608266,
                    779912, 950103, 211756, 665028, 642996, 262173, 789020, 932421, 390745, 433434, 350262, 463568,
                    668809, 305781, 815771, 550800 });

            //163578911
        }
    }
}
