﻿using System;

namespace RecursiveDigitSum
{
    class Program
    {
        static int digitSum(string n, int k)
        {
            if(n.Length == 1)
            {
                return Int32.Parse(n);
            }
            return digitSum()
        }

        static void Main(String[] args)
        {
            //string[] tokens_n = Console.ReadLine().Split(' ');
            //string n = tokens_n[0];
            //int k = Convert.ToInt32(tokens_n[1]);
            int result = digitSum("148", 3);
            Console.WriteLine(result);
        }
    }
}
