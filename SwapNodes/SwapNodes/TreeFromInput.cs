﻿namespace SwapNodes
{
    class TreeFromInput
    {
        Node root;
        public TreeFromInput()
        {
            root = null;
        }

        public void Insert(int[] arr, int length)
        {
            root = Insert(arr, length, 0);
        }

        public Node Insert(int[] arr, int length, int index)
        {
            if (index >= length || arr[index] == -1)
                return null;

            Node tmp = new Node(arr[index]);
            tmp.Left = Insert(arr, length, 2 * arr[index] - 1);
            tmp.Right = Insert(arr, length, 2 * arr[index]);

            return tmp;
        }
    }
}
