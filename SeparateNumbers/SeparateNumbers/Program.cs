﻿using System;

namespace SeparateNumbers
{
    class Program
    {
        static string separateNumbers(string s)
        {
            bool isSeparatedNumbers = false;
            int firstPart;
            int secondPart;
            int savedFirstPart = 0;
            int startIdx = 0;

            bool endWithNine = false;

            for (int range = 1; range <= s.Length / 2; range++)
            {
                while (startIdx + range != s.Length)
                {
                    firstPart = 0;
                    secondPart = 0;

                    Int32.TryParse(s.Substring(startIdx, range).ToString(), out firstPart);

                    if (firstPart.ToString().EndsWith("9"))
                    {
                        range++;
                        endWithNine = true;
                    }
                    startIdx = s.LastIndexOf(firstPart.ToString()) +  1;

                    Int32.TryParse(s.Substring(startIdx, range).ToString(), out secondPart);

                    startIdx = s.LastIndexOf(secondPart.ToString());

                    if (firstPart + 1 != secondPart)
                    {
                        break;
                    }
                    else
                    {
                        if (savedFirstPart == 0)
                        {
                            savedFirstPart = firstPart;
                        }
                        if (s.EndsWith(secondPart.ToString()))
                        {
                            isSeparatedNumbers = true;

                            break;
                        }
                    }
                }
                startIdx = 0;
            }
            return isSeparatedNumbers ? "YES " + savedFirstPart.ToString() : "NO";
        }


        static void Main(String[] args)
        {
            //int q = Convert.ToInt32(Console.ReadLine());
            //for (int a0 = 0; a0 < q; a0++)
            //{
            //    string s = Console.ReadLine();
            //    separateNumbers(s);""
            //}1
            separateNumbers("91011");
        }
    }
}
