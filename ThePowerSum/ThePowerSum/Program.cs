﻿using System;
using System.Collections;
using System.Linq;

namespace ThePowerSum
{
    class Program
    {
        static int powerSum(int X, int N)
        {
            return calc(X, N, 1);
        }
        static int calc(int x, int n, int num)
        {
            int numToN = (int)Math.Pow(num, n);
            if (numToN > x)
            {
                return 0;
            }
            else if (numToN == x)
            {
                return 1;
            }
            else
            {
                return (calc(x, n, num + 1) + calc(x - numToN, n, num + 1));
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine(powerSum(100, 2));
            Console.ReadLine();
        }
    }
}
