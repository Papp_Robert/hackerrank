﻿using System;
using System.Collections.Generic;
using System.Linq;
/// <summary>
/// https://www.hackerrank.com/challenges/lonely-integer/problem
/// </summary>
///
namespace LonelyInteger
{
    class Program
    {
        static int lonelyinteger(int[] a)
        {
            //Dictionary<int, int> counts = new Dictionary<int, int>();            
            //for (int i = 0; i < a.Length; i++)
            //{
            //    if(!counts.ContainsKey(a[i]))
            //    {
            //        counts.Add(a[i], 1);
            //    }
            //    else
            //    {
            //        int temp = counts[a[i]];
            //        counts[a[i]] = temp + 1;
            //    }
            //}
            //var lonelyInt =  counts.SingleOrDefault(x => x.Value ==  1);
            //return lonelyInt.Key;
            int temp = 0;
            for (int i = 0; i < a.Length; i++)
            {
                temp = temp ^ a[i];
            }
            return temp;
        }

        static void Main(String[] args)
        {
            //int n = Convert.ToInt32(Console.ReadLine());
            //string[] a_temp = Console.ReadLine().Split(' ');
            //int[] a = Array.ConvertAll(a_temp, Int32.Parse);
            int result = lonelyinteger(new int[] { 1, 2, 1, 2, 3 });
            Console.WriteLine(result);
        }
    }
}
