﻿using System;
using System.Text;

namespace RichieRich
{
    /// <summary>
    /// https://www.hackerrank.com/challenges/richie-rich/problem
    /// </summary>
    class Program
    {
        static string richieRich(string s, int n, int k)
        {
            const string badInput = "-1";
            bool isInCorrectForm = true;
            int difCount = 0;
            char[] charArrayFromString = s.ToCharArray();
            var originalString = s.ToCharArray();

            var maximizedString = new string(charArrayFromString);

            string firstPart = maximizedString.Substring(0, n / 2);
            string secondPart = maximizedString.Substring(n / 2, n / 2);
            int j = (n / 2) - 1;

            for (int i = 0; i < firstPart.Length; i++)
            {
                if (firstPart[i] != secondPart[j])
                {
                    if (firstPart[i] > secondPart[j])
                    {
                        charArrayFromString[j + (n / 2)] = firstPart[i];
                    }
                    else
                    {
                        charArrayFromString[i] = secondPart[j];
                    }
                    difCount++;
                }
                j--;
            }

            if (difCount > k)
            {
                isInCorrectForm = false;
            }
            else if (k >= 2)
            {
                int idx = 0;
                int jdx = 1;
                bool isStillTheSame = true;
                while (k > 1 && isStillTheSame)
                {
                    charArrayFromString[idx] = '9';
                    charArrayFromString[n - jdx] = '9';
                    jdx++;
                    idx++;
                    k -= 2;

                    var checkAbleString = new string(charArrayFromString);
                    isStillTheSame = checkAbleString.Substring(0, n / 2) == checkAbleString.Substring(n / 2, n / 2);
                }
            }
            if (k > 0 && (n / 2 != 0))
            {
                charArrayFromString[(n - n / 2) - 1] = '9';
            }

            return isInCorrectForm ? new string(charArrayFromString) : badInput;
        }

        static void Main(string[] args)
        {
            string s = "1191111";
            int n = 7;
            int k = 4;
            Console.WriteLine(richieRich(s, n, k));
            Console.ReadLine();
        }
    }
}
