﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SmallestPositive
{
    class Program
    {

        public static int solution(int [] A)
        {
            int i = 1;
            A = A.Distinct().OrderBy(x => x).ToArray();
            int max = A.Max();

            foreach (var number in A)
            {
                if (number == i)
                {
                    i++;
                }
                else
                {
                    return i;
                }
            }
            return 0;
        }
        static void Main(string[] args)
        {
            solution(new int[] { 1, 3, 6, 3, 1, 2 });
            Console.ReadLine();
        }
    }
}
