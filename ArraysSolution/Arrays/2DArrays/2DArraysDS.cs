﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Arrays._2DArrays
{
    public class TwoDimensionalArrays
    {
        List<int> sums = new List<int>();

        public int CalculateHourGlasses(int[][] arr)
        {
            for (int i = 0; i < arr.Length - 2; i++)
            {
                for (int j = 0; j < arr[i].Length - 2; j++)
                {
                    sums.Add(CalculateSum(arr[i][j], arr[i][j + 1], arr[i][j + 2], arr[i + 1][j + 1], arr[i + 2][j], arr[i + 2][j + 1], arr[i + 2][j + 2]));
                }
            }

            return sums.Max();
        }

        private int CalculateSum(int v1, int v2, int v3, int v4, int v5, int v6, int v7)
        {
            return v1 + v2 + v3 + v4 + v5 + v6 + v7;
        }
    }


}
