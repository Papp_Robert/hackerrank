﻿using System;
using Arrays._2DArrays;

namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            TwoDimensionalArrays two = new TwoDimensionalArrays();
            int[][] arr = new int[6][];

            arr[0] = new int[] { -9, -9, -9, 1, 1, 1 };
            arr[1] = new int[] { 0, -9, 0, 4, 3, 2 };
            arr[2] = new int[] { -9, -9, -9, 1, 2, 3 };
            arr[3] = new int[] { 0, 0, 8, 6, 6, 0 };
            arr[4] = new int[] { 0, 0, 0, -2, 0, 0 };
            arr[5] = new int[] { 0, 0, 1, 2, 4, 0 };

            Console.WriteLine(two.CalculateHourGlasses(arr));
            Console.WriteLine("Hello World!");
            Console.ReadLine();
        }
    }
}
