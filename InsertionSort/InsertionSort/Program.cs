﻿using System;

namespace InsertionSort
{
    class Program
    {
        static void insertionSort1(int n, int[] arr)
        {
            int insertableElementIdx = n - 1;
            var insertableElement = arr[insertableElementIdx];
            bool sorted = false;

            for (int i = insertableElementIdx - 1; i >= 0; i--)
            {
                var comperableElement = arr[i];
                if (comperableElement > insertableElement)
                {
                    arr[i + 1] = comperableElement;
                }
                else
                {
                    arr[i + 1] = insertableElement;
                    foreach (var item in arr)
                    {
                        Console.Write(item + " ");
                    }
                    Console.WriteLine();
                    break;
                }
                foreach (var item in arr)
                {
                    Console.Write(item + " ");
                }
                Console.WriteLine();
            }
            if(arr[0] > insertableElement)
            {
                arr[0] = insertableElement;

                foreach (var item in arr)
                {
                    Console.Write(item + " ");
                }
                Console.WriteLine();
            }
        }

        static void Main(String[] args)
        {
            //int n = Convert.ToInt32(Console.ReadLine());
            //string[] arr_temp = Console.ReadLine().Split(' ');
            //int[] arr = Array.ConvertAll(arr_temp, Int32.Parse);
            insertionSort1(5, new int[] { 2, 3, 4, 6, 1 });
            //insertionSort1(10, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 1 });
            Console.ReadLine();
        }
    }
}
