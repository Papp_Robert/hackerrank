﻿using System;

namespace SumVsXor
{
    class Program
    {
        static long solve(long n)
        {
            //int count = 1;
            //for (int i = 1; i < n; i++)
            //{
            //    var x = i & n;
            //    if (x == 0)
            //    {
            //        count++;
            //    }
            //}
            //return count;
            int unset_bits = 0;
            while (n > 0)
            {
                if ((n & 1) == 0)
                {
                    unset_bits++;
                }
                n = n >> 1;
            }
            return 1L << unset_bits;
        }

        static void Main(String[] args)
        {
            //long n = Convert.ToInt64(Console.ReadLine());
            long result = solve(1099511627776);
            Console.WriteLine(result);

            Console.ReadLine();
        }

    }
}
