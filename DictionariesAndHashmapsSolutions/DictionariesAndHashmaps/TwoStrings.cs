﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DictionariesAndHashmaps
{
    class TwoStrings
    {
        public string TwoStringsMethod (string s1, string s2)
        {
            bool isSubstring = false;

            foreach (var s1Char in s1)
            {
                if (s2.Contains(s1Char.ToString()))
                {
                    isSubstring = true;
                    break;
                }
            }

            return isSubstring ? "YES" : "NO";
        }

    }
}
