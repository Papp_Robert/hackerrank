﻿using System;

namespace PeriodOfSmallestIntegers
{
    class Program
    {
        public static int solution(int n)
        {
            int[] d = new int[30];
            int l = 0;
            int p;
            while (n > 0)
            {
                d[l] = n % 2;
                n /= 2;
                l++;
            }
            for (p = 1; p < 1 + l; ++p)
            {
                int i;
                bool ok = true;
                for (i = 0; i < l - p; ++i)
                {
                    if (d[i] != d[i + p])
                    {
                        ok = false;
                        break;
                    }
                }
                if (ok)
                {
                    return p;
                }
            }
            return -1;
        }
//        select sum(count) over(order by time)
            //from(
            //    select start_time as time, 1 as count
            //    from meetings
            //    union all
            //    select end_time, -1
            //    from meetings
            //    ) s
            //order by 1 desc
            //limit 1;

        static void Main(string[] args)
        {
            solution(955);
            Console.ReadLine();
        }
    }
}
