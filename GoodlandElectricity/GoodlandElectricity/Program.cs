﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GoodlandElectricity
{
    class Program
    {

        static int pylons(int range, int[] arr)
        {
            const int cannotBuildElectiricityInAllCities = -1;

            int minimumCountOfElectricity = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == 1)
                {
                    BackWardRange(range, arr, i);
                    ForwardRange(range, arr, i);
                }
            }

            List<int> zeroElementsIndex = new List<int>();

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == 0)
                {
                    zeroElementsIndex.Add(i);
                    minimumCountOfElectricity++;
                }
            }

            return minimumCountOfElectricity;
        }

        private static void BackWardRange(int range, int[] arr, int i)
        {
            bool canPutElectricityBackward = true;
            int j = i - 1;
            int rangeTmp = range;

            while (canPutElectricityBackward)
            {
                if (j <= 0 || rangeTmp == 1) //AT the beginning
                {
                    canPutElectricityBackward = false;
                    j = 0;
                }

                arr[j] = 1;
                j--;
                rangeTmp--;
            }
        }

        private static void ForwardRange(int range, int[] arr, int i)
        {
            bool canPutElectricityForward = true;
            int j = i + 1;
            int rangeTmp = range;

            while (canPutElectricityForward)
            {
                if (j >= arr.Length - 1 || rangeTmp == 1) //AT the end
                {
                    canPutElectricityForward = false;
                    j = arr.Length - 1;
                }

                arr[j] = 1;
                j++;
                rangeTmp--;
            }
        }

        static void Main(string[] args)
        {
            //this should return 3.
            pylons(20, new int[] { 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0 });
            Console.WriteLine("Hello World!");
        }
    }
}
