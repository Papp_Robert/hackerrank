﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperReducedString
{
    /// <summary>
    /// https://www.hackerrank.com/challenges/reduced-string/problem
    /// </summary>
    class Program
    {
        static string super_reduced_string(string s)
        {
            Stack<char> stack = new Stack<char>();
            foreach (char c in s)
            {
                char top = (stack.Count == 0) ? ' ' : stack.Peek();
                if (top == c)
                {
                    stack.Pop();
                }
                else
                {
                    stack.Push(c);
                }
            }
            char[] reduced = stack.ToArray();
            Array.Reverse(reduced);
            if (reduced.Length == 0)
            {
                return "Empty String";
            }
            else
            {
                return (new String(reduced));
            }
        }
        static void Main(string[] args)
        {
            super_reduced_string("aaabccddd");
        }
    }
}
