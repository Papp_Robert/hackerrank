﻿using System;

namespace CrosswordPuzzle
{
    class Program
    {
        private static string[] results = new string[10];
        private static int i = 0;
        private static int j = 0;

        static void crosswordPuzzle(string crossword, string hints, string result, ref int num)
        {
            if (result.Length == 10)
            {
                results[num] = result;
                num++;
                i = 0;
                result = string.Empty;
            }

            foreach (var wordChar in crossword)
            {
                string tempResult = "";

                if (wordChar != '-')
                {
                    tempResult = result + wordChar;
                }
                else
                {
                    tempResult = result + hints[num];
                    hints = hints.Substring(1);
                }
                crossword = crossword.Substring(1);
                i++;
                crosswordPuzzle(crossword, hints, tempResult, ref num);
            }
        }

        static void Main(String[] args)
        {
            string[] crossword = new string[10];
            crossword = new string[]
            {
                "+-++++++++",
                "+-++++++++",
                "+-++++++++",
                "+-----++++",
                "+-+++-++++",
                "+-+++-++++",
                "+++++-++++",
                "++------++",
                "+++++-++++",
                "+++++-++++"
            };
            int num = 0;
            string hints = "LONDON; DELHI; ICELAND; ANKARA;";
            for (int i = 0; i < 10; i++)
            {
                crosswordPuzzle(crossword[i], hints, "", ref num);
            }
            
        }
    }
}
