﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HackerrankInString
{
    class Program
    {
        static string hackerrankInString(string s)
        {
            bool contains = true;
            string hrk = "hackerrank";

            int i = 0;

            foreach (var item in hrk)
            {
                i = s.IndexOf(item);

                if (i == -1)
                {
                    contains = false;
                    break;
                }
                s = s.Substring(i);
                i++;
            }

            return contains ? "YES" : "NO";

            //var hackerrank = new Queue<char>("hackerrank".ToCharArray());
            //foreach (char c in s)
            //{
            //    if (c == hackerrank.Peek())
            //        hackerrank.Dequeue();

            //    if (hackerrank.Count == 0)
            //        return "YES";
            //}

            //return "NO";
        }

        static void Main(String[] args)
        {

            string s = "hereiamstackerrank";
            string result = hackerrankInString(s);
            Console.WriteLine(result);

        }
    }
}
