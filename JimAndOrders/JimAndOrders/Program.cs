﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JimAndOrders
{
    class Program
    {
        static int[] jimOrders(int[][] orders)
        {
           int[] sums = new int[orders.Length];
            int idx = 0;

            for (int i = 0; i < orders.Length; i++)
            {
                for (int j = 0; j < orders[i].Length; j++)
                {
                    sums[idx] += orders[i][j];
                }
                idx++;

            }

            return sums.Select((x, i) => new { Count = x, index = i + 1 })
                        .OrderBy(x => x.Count)
                        .Select(x => x.index).ToArray();

                        
        }
        static void Main(string[] args)
        {
            int[][] arr = new int[5][];
            arr[0] = new int[2] { 8, 1 };
            arr[1] = new int[2] { 4, 2 };
            arr[2] = new int[2] { 5, 6 };
            arr[3] = new int[2] { 3, 1 };
            arr[4] = new int[2] { 4, 3 };

            foreach (var item in jimOrders(arr))
            {
                Console.WriteLine(item);
            }
            

            Console.ReadLine();
        }
    }
}
