﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaximizingXor
{
    /// <summary>
    /// https://www.hackerrank.com/challenges/maximizing-xor/problem
    /// </summary>
    class Program
    {
        static int maximizingXor(int l, int r)
        {
            List<int> xors = new List<int>();
            for (int i = l; i <= r; i++)
            {
                for (int j = l; j <= r; j++)
                {
                    xors.Add(i ^ j);
                }
            }
            return xors.Max();
        }

        static void Main(String[] args)
        {
            int result = maximizingXor(10, 15);
            Console.WriteLine(result);

            Console.ReadLine();
        }
    }
}
