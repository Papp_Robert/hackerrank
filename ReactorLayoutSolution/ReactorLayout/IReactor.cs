﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReactorLayout
{
    interface IReactor
    {
        double Width { get; }

        double Height { get; }

        double X { get; }

        double Y { get; }
    }

}
