﻿using System;

/// <summary>
/// https://www.hackerrank.com/challenges/queens-attack-2/problem
/// </summary>

class Solution
{
    static int queensAttack(int n, int k, int r_q, int c_q, int[][] obstacles)
    {
        int count = 0;
        const int notFreeChessField = 0;
        const int freeChessField = 1;
        const int queenChessField = 10;
        const int obstaclesField = 100;
        int[][] chessPlay = new int[n][];

        r_q -= 1;
        c_q -= 1;

        for (int i = 0; i < n; i++)
        {
            chessPlay[i] = new int[n];

            for (int j = 0; j < n; j ++)
            {
                chessPlay[i][j] = freeChessField;
            }
        }
        chessPlay[r_q][c_q] = queenChessField;

        for (int z = 0; z < obstacles.Length; z++)
        {
            var element = obstacles[z];
            var row = element[0] - 1;
            var line = element[1] - 1;
            chessPlay[row][line] = obstaclesField;

            if (row == r_q && line == c_q)
            {
                for (int i = 0; i <= row; i++)
                {
                    chessPlay[row - i][line] = obstaclesField;
                }

                for (int i = 0; i <= line; i++)
                {
                    chessPlay[row][line - i] = obstaclesField;
                }

                for (int i = 0; i <= row; i++)
                {
                    if (row - i >= 0 && line - i >= 0)
                    {
                        chessPlay[row - i][line - i] = obstaclesField;
                    }
                    if (row + i < n && line + i < 0)
                    {
                        chessPlay[row + i][line + i] = obstaclesField;
                    }
                }
            }
        }

        for (int row = 0; row < n; row++)
        {
            for (int column = 0; column < n; column++)
            {
                if (row == r_q && chessPlay[row][column] != queenChessField && chessPlay[row][column] != obstaclesField)
                {
                    chessPlay[row][column] = notFreeChessField;
                }
                if (column == c_q && chessPlay[row][column] != queenChessField && chessPlay[row][column] != obstaclesField)
                {
                    chessPlay[row][column] = notFreeChessField;
                }
            }
        }

        //CROSS
        for (int z = 1; z < n; z++)
        {
            if (r_q - z >= 0 && c_q - z >= 0)
            {
                if (chessPlay[r_q - z][c_q - z] != notFreeChessField && chessPlay[r_q - z][c_q - z] != obstaclesField)
                {
                    chessPlay[r_q - z][c_q - z] = notFreeChessField;
                }
            }
        }

        for (int z = 1; z < n; z++)
        {
            if (r_q - z >= 0 && c_q + z < n)
            {
                if (chessPlay[r_q - z][c_q + z] != notFreeChessField && chessPlay[r_q - z][c_q + z] != obstaclesField)
                {
                    chessPlay[r_q - z][c_q + z] = notFreeChessField;
                }
            }
        }

        for (int z = 1; z < n; z++)
        {
            if (r_q + z < n && c_q + z < n)
            {
                if (chessPlay[r_q + z][c_q + z] != notFreeChessField && chessPlay[r_q - z][c_q - z] != obstaclesField)
                {
                    chessPlay[r_q + z][c_q + z] = notFreeChessField;
                }
            }
        }

        for (int z = 1; z < n; z++)
        {
            if (r_q + z < n && c_q - z >= 0)
            {
                if (chessPlay[r_q + z][c_q - z] != notFreeChessField && chessPlay[r_q - z][c_q - z] != obstaclesField)
                {
                    chessPlay[r_q + z][c_q - z] = notFreeChessField;
                }
            }
        }


        foreach (var items in chessPlay)
        {
            foreach (var item in items)
            {
                if (item == notFreeChessField)
                {
                    count++;
                }
            }
        }
        return count;
    }

    static void Main(String[] args)
    {
        var a = new int[][] { new int[] { 5, 5 }, new int[] { 4, 2 }, new int[] { 2, 3 } };
        int result = queensAttack(5, 3, 5, 4, a);
        Console.WriteLine(result);
    }
}
