﻿using System;
using System.Text;

namespace FlippingBitsProblem
{
    class Program
    {
        static long flippingBits(long N)
        {
            var binary = Convert.ToString(N, 2);
            binary = binary.PadLeft(32, '0');

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < binary.Length; i++)
            {
                if(binary[i] == '0')
                {
                    sb.Append('1');
                }
                else
                {
                    sb.Append('0');
                }
            }

            return Convert.ToInt64(sb.ToString(), 2);
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Console.WriteLine(flippingBits(2147483647));

            Console.ReadLine();
        }
    }
}
