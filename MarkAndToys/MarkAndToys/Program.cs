﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkAndToys
{
    class Program
    {
        static int maximumToys(int[] prices, int k)
        {
            int count = 0;
            bool canAfford = true;
            var sortedElements = prices.OrderBy(x => x).ToArray();
            int i = 0;

            do
            {
                var min = sortedElements.ElementAt(i);
                if (min <= k)
                {
                    count++;
                    k -= min;
                    i++;
                }
                else
                {
                    canAfford = false;
                }

            } while (canAfford);

            return count;
        }

        static void Main(String[] argsg){
            //    string[] tokens_n = Console.ReadLine().Split(' ');
            //    int n = Convert.ToInt32(tokens_n[0]);
            //    int k = Convert.ToInt32(tokens_n[1]);
            //    string[] prices_temp = Console.ReadLine().Split(' ');
            //    int[] prices = Array.ConvertAll(prices_temp, Int32.Parse);
            int[] prices = new int[] { 1, 12, 5, 111, 200, 1000, 10 };
            int result = maximumToys(prices, 50);
            Console.WriteLine(result);
        }
    }
}
