﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace ExtraLongFactorials
{
    class Program
    {
        public static Dictionary<int, BigInteger> facts;
        static BigInteger extraLongFactorials(int n)
        {           
            for (int i = 2; i <= n; i++)
            {
                if (!facts.ContainsKey(i))
                {
                    facts[i] = Factorial(i);
                }
            }
            return facts[n];
        }

        static BigInteger Factorial(int n)
        {
            if (n == 0)
            {
                return 1;
            }
            if (n == 1)
            {
                return 1;
            }
            return n * Factorial(n - 1);
        }
        static void Main(string[] args)
        {
            int calculateFibTill = 45;
            facts = new Dictionary<int, BigInteger>();
            facts.Add(0, 1);
            facts.Add(1, 1);

            //Console.WriteLine(Factorial(4));

            Console.WriteLine(extraLongFactorials(calculateFibTill));
            Console.ReadLine();
        }
    }
}
