﻿using System.Collections.Generic;
using System.Linq;

namespace RecursiveReverseString
{
    class ReverseStringClass
    {
        public IEnumerable<char> ReverseStringWithBuiltInReverse(string input)
        {
            return input.Reverse();
        }

        public string ReverseStringsWithCycle(string input)
        {
            string reversedInput = "";
            for (int i = input.Length - 1; i >= 0; i--)
            {
                reversedInput += input[i];
            }

            return reversedInput;
        }

        public string ReverseStringWithRecursion(string input)
        {
            if (input.Length > 0)
            {
                return input[input.Length - 1] + ReverseStringWithRecursion(input.Substring(0, input.Length -1));
            }
            else
            {
                return input;
            }
        }
    }
}
