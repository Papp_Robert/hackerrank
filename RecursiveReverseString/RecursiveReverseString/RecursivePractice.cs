﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RecursiveReverseString
{
    class RecursivePractice
    {
        static Dictionary<Char, String> DigitMap = new Dictionary<Char, String>()
        {
         {'0', "0"},
         {'1', "1"},
         {'2', "ABC"},
         {'3', "DEF"},
         {'4', "GHI"},
         {'5', "JKL"},
         {'6', "MNO"},
         {'7', "PQRS"},
         {'8', "TUV"},
         {'9', "WXYZ"}
        };

        public static List<string> Combinations = new List<string>();


        public int FactorialRecursive(int factorialNum)
        {
            //EXIT condition
            if (factorialNum == 1)
            {
                return 1;
            }
            return factorialNum * FactorialRecursive(factorialNum - 1);
        }


        public float Power(float num, int powerOf)
        {
            if (powerOf == 1)
            {
                return num;
            }
            return num * Power(num, powerOf - 1);
        }

        public void CalculatePhoneNumbers(int[] nums, int level)
        {
            if (level >= nums.Count())
            {
                Combinations.Add(string.Join("", nums));
            }
            for (int i = level; i < nums.Count(); i++)
            {
                Swap(ref nums[level], ref nums[i]);
                CalculatePhoneNumbers(nums, level + 1);
                Swap(ref nums[level], ref nums[i]);
            }
        }

        private static void Swap<T>(ref T item1, ref T item2)
        {
            T temp = item1;
            item1 = item2;
            item2 = temp;
        }
    }
}


