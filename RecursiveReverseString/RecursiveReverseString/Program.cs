﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecursiveReverseString
{
    class Program
    {
        static void Main(string[] args)
        {
            ReverseStringClass reverse = new ReverseStringClass();
            RecursivePractice recursionPractice = new RecursivePractice();
            //var hello = "hello";
            //Console.WriteLine(reverse.ReverseStringsWithCycle("hello"));
            //Console.WriteLine(reverse.ReverseStringWithBuiltInReverse("hello"));

            //Console.WriteLine(reverse.ReverseStringWithRecursion(hello));
            Console.WriteLine(recursionPractice.Power(2, 3));
            //recursionPractice.CalculatePhoneNumbers(new int[] { 1, 2, 3 }, 0);
            //foreach (var item in RecursivePractice.Combinations)
            //{
            //    Console.WriteLine(item);
            //}
            Permutations perm = new Permutations();
            perm.InputSet = perm.MakeCharArray("4210");
            perm.CalcPermutation(0);


            Console.ReadLine();
        }
    }
}
