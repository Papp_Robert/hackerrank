﻿using System;
using System.Linq;

namespace BiggerIsGreaterNamespace
{
    class Program
    {
        private static string Bigger(string w)
        {
            var wList = w.ToList();

            for (int i = wList.Count - 1; i > 0; i--)
            {
                if (w[i] == 0)
                {
                    return "No answer";
                }
                if (w[i] > w[i - 1])
                {
                    var temp = wList[i];
                    wList[i] = wList[i - 1];
                    wList[i - 1] = temp;

                    break;
                }
            }

            return string.Join("", wList);
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Bigger("hefg");
        }
    }
}
