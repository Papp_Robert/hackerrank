﻿using System;
using System.Linq;

namespace Swap
{
    class Program
    {
        public static bool solution(int[] A)
        {
            int swapNumbers = 0;
            int swappedToThisElementIndex = 0;

            for (int i = 0; i < A.Length - 1; i++)
            {
                if (A[i] > A[i + 1])
                {
                    for (int j = i + 1; j < A.Length; j++)
                    {
                        if (A[j] > A[i + 2])
                        {
                            swappedToThisElementIndex = j -1;
                        }
                    }

                    int temp = A[swappedToThisElementIndex];
                    A[swappedToThisElementIndex] = A[i];
                    A[i] = temp;

                    swapNumbers++;
                    i = 0;
                }
                if (swapNumbers > 1)
                {
                    return false;
                }
            }

            return true;
        }

        static void Main(string[] args)
        {
            solution(new int[] { 1, 5, 3, 3, 7 });
            Console.WriteLine("Hello World!");
        }
    }
}
