﻿using System;
using System.Linq;

class Solution
{

    static int toys(int[] w)
    {
        int containerNums = 0;
        var sorted = w.OrderBy(x => x).ToArray();


        while(sorted.Length != 0)
        {
            var firstElement = sorted.First();

            sorted = sorted.Where(x => x > firstElement + 4).ToArray();
            containerNums++;

        }

        return containerNums;
    }

    static void Main(String[] args)
    {
        int[] w = new int[] { 1, 2, 3, 21, 7, 12, 14, 21 };
        int result = toys(w);
        Console.WriteLine(result);
    }
}
