﻿using System;
using System.Collections.Generic;

public class WindowManager
{
    private string[] arr;

    private int topOfStack;

    public WindowManager()
    {
        arr = new string[10];
        topOfStack = 0;
    }
    public void Open(string windowName)
    {
        if (topOfStack > arr.Length - 1)
        {
            Array.Resize(ref arr, topOfStack + 1);
            arr[topOfStack] = windowName;
        }
        else
        {
            arr[topOfStack] = windowName;
        }
        topOfStack++;
    }
        

    public void Close(string windowName)
    {
        for (int i = 0; i < arr.Length; i++)
        {
            if(arr[i].Equals(windowName))
            {
                arr[i] = null;
                for (int j = i; j < arr.Length; j++)
                {
                    if (arr[j] != null)
                    {
                        arr[j - 1] = arr[j];
                        arr[j] = null;
                        topOfStack--;
                    }
                }
                break;

            }
        }               
    }

    public string GetTopWindow()
    {
        return arr[topOfStack -1];
    }

    public static void Main(string[] args)
    {
        WindowManager wm = new WindowManager();
        wm.Open("Calculator");
        wm.Open("Browser");
        wm.Open("Player");
        wm.Close("Browser");
        wm.Close("Player");

        Console.WriteLine(wm.GetTopWindow());
        Console.ReadLine();
    }
}