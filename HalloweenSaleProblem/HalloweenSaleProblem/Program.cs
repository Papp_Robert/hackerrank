﻿using System;

namespace HalloweenSaleProblem
{
    class Program
    {

        static int howManyGames(int price, int discount, int minimum, int sum)
        {
            int boughtGamesCount = 0;
            if(price > sum)
            {
                return 0;
            }
            while(sum >= price)
            {
                boughtGamesCount++;
                sum -= price;
                price -= discount;

                if (price <= minimum)
                {
                    price = minimum;
                }
                
            }

            return boughtGamesCount;

        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            howManyGames(100, 19, 1, 180);
        }
    }
}
