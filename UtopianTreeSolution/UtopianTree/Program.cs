﻿using System;

namespace UtopianTree
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine(utopianTree(5));

            Console.ReadLine();
        }

        // Complete the utopianTree function below.
        static int utopianTree(int n)
        {
            int result = 1;

            for (int i = 0; i < n; i++)
            {
                if (i % 2 == 0)
                {
                    result *= 2;
                }
                else
                {
                    result += 1;
                }
            }
            return result;
        }
    }
}
