﻿using System;
using System.Text;

namespace SherlockAndTheBeast
{
    class Program
    {
        public static string DecentNumber(int n)
        {
            StringBuilder sb = new StringBuilder();


            if (n % 3 == 0)
            {
                for (int i = 0; i < n; i++)
                {
                    sb.Append("5");
                }
                return sb.ToString();
            }

            if (n < 5)
            { 
                return "-1";
            }

            for (int i = 0; i < n; i++)
            {
                sb.Append("5");
            }

            do
            {
                if (n - 5 >= 0)
                {
                    sb.Replace('5', '3', n - 5, 5);
                    n -= 5;
                }
                else
                {
                    return "-1";
                }
            } while (n % 3 != 0);

            return sb.ToString();
        }
        static void Main(string[] args)
        {
            //Console.WriteLine(DecentNumber(1));
            //Console.WriteLine(DecentNumber(2));
            //Console.WriteLine(DecentNumber(3));
            //Console.WriteLine(DecentNumber(4));
            //Console.WriteLine(DecentNumber(5));
            //Console.WriteLine(DecentNumber(6));
            Console.WriteLine(DecentNumber(7));
            Console.WriteLine(DecentNumber(8));
            Console.WriteLine(DecentNumber(9));
            Console.WriteLine(DecentNumber(10));
            Console.ReadLine();
        }
    }
}
