﻿using System;

namespace FibonacciWithTuple
{
    class Program
    {
        static void Main(string[] args)
        {
            FibonacciCalculate fib = new FibonacciCalculate();
            Console.WriteLine(fib.Fibonacci(10));

            Console.WriteLine(fib.FiboRecursive(10));

            Console.ReadLine();
        }
    }

    class FibonacciCalculate
    {
        public int Fibonacci(int x)
        {
            if (x < 0)
            {
                throw new ArgumentException("Less negativity please!", nameof(x));
            }
            return Fib(x).previous;

            (int current, int previous) Fib(int i)
            {
                if (i == 0)
                {
                    return (1, 0);
                }
                var (curr, prev) = Fib(i - 1);
                return (curr + prev, curr);
            }
        }

        public int FiboRecursive(int x)
        {
            if(x == 0)
            {
                return 0;
            }
            else if (x <= 1)
            {
                return 1;
            }
            return FiboRecursive(x - 2) + FiboRecursive(x - 1);
        }
    }
}
