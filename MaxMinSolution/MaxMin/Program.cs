﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// https://www.hackerrank.com/challenges/angry-children/problem
/// </summary>
/// 

namespace MaxMin
{
    class Program
    {
        static int angryChildren(int k, int[] arr)
        {
            var sortedElements = arr.OrderBy(x => x).ToArray();
            int diff = Int32.MaxValue ;

            for (int i = 0; i < sortedElements.Length; i ++)
            {
                if (i + k -1 < sortedElements.Length)
                {
                    int firstElement = sortedElements[i];
                    int secondElement = sortedElements[i + k -1];
                    int difference = secondElement - firstElement;


                    if (diff > difference)
                    {
                        diff = difference;
                    }
                }
            }

            return diff;
        }

        static void Main(string[] args)
        {
            //int angryChild = angryChildren(5, new int[] { 4504, 1520, 5857, 4094, 4157, 3902, 822, 6643, 2422, 7288, 8245, 9948, 2822, 1784, 7802, 3142, 9739, 5629, 5413, 7232 });
            int angryChild = angryChildren(3, new int[] { 100, 200, 300, 350, 400, 401, 402 });
            Console.ReadLine();
        }
    }
}
