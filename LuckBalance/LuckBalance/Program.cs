﻿using System;
using System.Linq;

namespace LuckBalance
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            int[][] contests = new int[6][];
            contests[0] = new[] { 5, 1 };
            contests[1] = new[] { 2, 1 };
            contests[2] = new[] { 1, 1 };
            contests[3] = new[] { 8, 1 };
            contests[4] = new[] { 10, 0 };
            contests[5] = new[] { 5, 0 };

            luckBalance(3, contests);
        }

        ///https://www.hackerrank.com/challenges/luck-balance/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=greedy-algorithms
        // Complete the luckBalance function below.
        static int luckBalance(int k, int[][] contests)
        {
            int luckBalance = 0;

            luckBalance += contests.Where(x => x[1] == 0).Sum(x => x[0]);
            var importantContests = contests.OrderByDescending(x => x[0]).Where(x => x[1] == 1).ToArray();

            for (int i = 0; i < importantContests.Length; i++)
            {
                if (k > 0)
                {
                    luckBalance += importantContests[i][0];
                }
                if (k <= 0)
                {
                    luckBalance -= importantContests[i][0];
                }
                k--;
            }

            return luckBalance;
        }
    }
}
